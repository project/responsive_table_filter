# Responsive Table input filter

Provides the Responsive Table transformation as an input filter.

## Usage

* Go to admin/config/content/formats and activate Responsive Table as an input 
  filter on a text format.
* Create a node using this filter enabled text format
* Insert a wide table with content to test it out.
